  var socket = io();

  $(function () {
   var $el = $("#draggable").draggable({
    drag: function (event, ui) {
     socket.emit('drag', ui.offset);
    }
   });

   socket.on('drag', function (data) {
    $el.offset(data);

   });

  });