// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(__dirname + '/public'));

// Chatroom

let numUsers=1;

io.on('connection', function (socket) {
  socket.on('addUser', function (data) {
    console.log('addUser');
    if(numUsers < 3){
     socket.username = numUsers;
    //  console.log(numUsers);
    //  console.log(socket.id);
    //  console.log(socket.client.id);
     socket.join('room1');
    } else if(numUsers<5) {
      socket.join('room2');
      console.log(socket.id);
    }
    numUsers++;
  });
  socket.on('drag', function (data) {
    socket.broadcast.emit('drag', data);
  });
    socket.on('tic-tac-toe', function (data) {
    console.log('tic-tac-toe',data);
    var clients = Object.keys(socket.rooms)[1];
    socket.broadcast.to(clients).emit('tic-tac-toe', data);
  });
});
